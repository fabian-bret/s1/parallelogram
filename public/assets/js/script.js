function compNumbers() {
	let base = Number(document.getElementById('base').value);
	let height = Number(document.getElementById('height').value);

	document.getElementById('result').innerHTML = base * height;
	return false;
}

document.getElementById('compute').addEventListener('click', compNumbers);

function clearNumbers() {
	document.getElementById('base').value = " ";
	document.getElementById('height').value = " ";

	document.getElementById('result').innerHTML = "Operation will appear here.";
	document.getElementById('shape').style.width =  "0px";
	document.getElementById('shape').style.height = "0px";
	return false;
}

document.getElementById('clear').addEventListener('click', clearNumbers);


function visual() {
	let base = Number(document.getElementById('base').value);
	let height = Number(document.getElementById('height').value);

	document.getElementById('shape').style.width = base + "px";
	document.getElementById('shape').style.height = height + "px";
	return false;
}

document.getElementById('compute').addEventListener('click', visual);